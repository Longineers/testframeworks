Feature: birthday
  In order to see if it's my birthday
  As a regular user
  I need to be able to call the isItMyBirthday() method
  
Scenario: Determine if it is not my birthday
  Given I have a birthdate of "May 12th"
  And today's date is "October 3rd"
  When I call "isItMyBirthday"
  Then it should display "No, today is not your birthday!"
