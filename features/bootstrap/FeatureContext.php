<?php

require __DIR__ . '/../birthday.php';
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
  private $sBirthDate;
  private $sTodaysDate;
  private $sOutput;
    /**
     * @Given I have a birthdate of :date
     */
    public function iHaveABirthdateOf($date)
    {
      $this->sBirthDate = $date;
    }

    /**
     * @Given today's date is :date
     */
    public function todaySDateIs($date)
    {
      $this->sTodaysDate = $date;
    }

    /**
     * @When I call :method
     */
    public function iCall($method)
    {
        $this->sOutput = $method($this->sBirthDate, $this->sTodaysDate);
    }

    /**
     * @Then it should display :message
     */
    public function itShouldDisplay($message)
    {
        if ( $message !== $this->sOutput ) {
          throw new Exception("Actual output is:{$this->sOutput}\n");
        }
    }
}
